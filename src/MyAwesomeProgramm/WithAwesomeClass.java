package MyAwesomeProgramm;

import java.io.*;
import java.util.ArrayList;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

public class WithAwesomeClass {
    public static String readFile (String fileName) {
        try {
            StringBuffer o = new StringBuffer();
            FileReader f = new FileReader(fileName);
            BufferedReader b = new BufferedReader(f);

            String s;
            while((s = b.readLine()) != null) {
                o.append(s + " ");
            }

            return o.toString();
        } catch (IOException e) {
            return e.getMessage();
        }
    }

    public static void saveList (String fileName, ArrayList<Token> tokens) {
        try {
            FileWriter f = new FileWriter(fileName);

            for(Token t : tokens) {
                f.write(t.toString() + "\n");
            }

            f.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static ArrayList<Token> getList(String in) {
        ArrayList<Token> tokenList = new ArrayList<Token>();

        //concatenate all Token types for Regex
        StringBuffer tokenBuffer = new StringBuffer();
        for(TokenType t : TokenType.values()) {
            // http://docs.oracle.com/javase/7/docs/api/java/util/regex/Pattern.html
            //Special constructs (named-capturing and non-capturing)
            //(?<name>X) 	X, as a named-capturing group
            if (t.getId() >= 0) //IGNORE some special blocks
                tokenBuffer.append(String.format("(?<%s>%s)|", t.name(), t.getPattern()));
        }

        //System.out.println(tokenBuffer.toString());

        Pattern pattern = Pattern.compile(tokenBuffer.toString());
        Matcher matcher = pattern.matcher(readFile(in));

        while(matcher.find()) {
            for(TokenType t : TokenType.values()) {
                if (t.getId() >= 0 && matcher.group(t.name()) != null) {
                    //Skiping whitespace
                    if (t.getId() == 0) {
                       continue;
                    }

                    tokenList.add(new Token(t, matcher.group(t.name())));
                }
            }
        }

        return tokenList;
    }

    public static void main (String a[]) {
        ArrayList<Token> tokenList = getList("input.txt");

        ExecutionLine executionLine = new ExecutionLine();

        Parser parser = new Parser(tokenList, executionLine);
        parser.parse();

        Interpreter interpreter = new Interpreter(executionLine);
        interpreter.execute();
        interpreter.printVars();
    }
}
