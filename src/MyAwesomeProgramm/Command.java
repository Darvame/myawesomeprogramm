package MyAwesomeProgramm;

public class Command extends Chunk {
    private int commandType;
    private String commandArg;

    Command(int c, String arg) {
        super();
        this.commandType = c;
        this.commandArg = arg;
    }

    public int getCommand() {
        return this.commandType;
    }

    public String getCommandArg() {
        return this.commandArg;
    }
}
