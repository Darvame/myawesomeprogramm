package MyAwesomeProgramm;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.ListIterator;

public class Interpreter {
    private HashMap<String, Double> variables;
    private ExecutionLine executionLine;
    private ListIterator<Command> commandList;

    private double get(String key) {
        if (this.variables.containsKey(key))
            return this.variables.get(key);

        return 0.0;
    }

    private void set(String key, double value) {
        this.variables.put(key, value);
    }

    Interpreter(ExecutionLine executionLine) {
        this.variables = new HashMap<String, Double>();
        this.executionLine = executionLine;
        this.commandList = executionLine.getCommandList();
    }

    public void printVars() {
        System.out.println(this.variables.toString());
    }

    private ArrayList<Token> getChunk(Token block) {
        return executionLine.getChunk(Integer.parseInt(block.getValue()));
    }

    //Procedure list:
    private void thisIsFunction(Command command) { // -- > 1
        ArrayList<Token> argList = command.getList();
        int size = argList.size();
        double args[] = new double[size];

        for (int i = 0; i < size; i++) {
            args[i] = inspectRightValue(getChunk(argList.get(i)));
        }

        for (int i = 0; i < size; i ++)
            System.out.print(args[i] + " ");

        System.out.println();
    }

    private void thisIsAssign(Command command) { // -- > 0
        this.set(command.getCommandArg(), inspectRightValue(command.getList()));
    }

    private boolean checkPriority(String v, boolean priority) {
        if (priority) {
            if (v.equals("*") || v.equals("/"))
                return true;

            return false;
        }

        return true;
    }

    //Binary
    double binary(String operation, double left, double right) {
        if (operation.equals("+")) return left + right;
        if (operation.equals("-")) return left - right;
        if (operation.equals("*")) return left * right;
        return left / right;
    }

    //Calc
    double calculate(ArrayList<Token> list) {
        boolean firstPriority = true;
        int beginSize = list.size();
        Token t;

        do {
            for (int l = 0; l < beginSize; l++) {
                for (int i = 0; i < list.size(); i++) {
                    t = list.get(i);

                    if (t.isValid(6) && checkPriority(t.getValue(), firstPriority)) {
                        list.get(i - 1).setOption(binary(t.getValue(), list.get(i - 1).getOption(), list.get(i + 1).getOption()));
                        list.remove(i);
                        list.remove(i);
                    }
                }
            }
            firstPriority = !firstPriority;
        } while (!firstPriority);

        return list.get(0).getOption();
    }

    //Inspect
    private double inspectRightValue(ArrayList<Token> list) {
        Token t;

        for (int i = 0; i < list.size(); i++) {
            t = list.get(i);
            if (t.isValid(-1)) {// 1 Inspect for chunks:
                double a = inspectRightValue(getChunk(t));
                list.set(i, new Token(TokenType.CONST, String.valueOf(a)));
                list.get(i).setOption(a);
            }else if (t.isValid(1)) {// 2 Inspect for variables:
                t.setOption(this.get(t.getValue()));
            }
            else if (t.isValid(4))
                t.setOption(Integer.parseInt(t.getValue()));
        }

        return calculate(list);
    }


    public void execute() {
        while(commandList.hasNext()) {
            Command command = commandList.next();

            if (command.getCommand() == 0)
                this.thisIsAssign(command);
            else if (command.getCommand() == 1)
                this.thisIsFunction(command);

            // .. else Unknown
        }
    }
}
