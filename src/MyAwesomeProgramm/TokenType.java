package MyAwesomeProgramm;

public enum TokenType {
    // List of my token types (Lexer rules)
    WHITESPACE(0, "\\s+"),
    LEFTBRACKET(2, "[(]"),
    RIGHTBRACKET(3, "[)]"),
    CONST(4, "-?[0-9.]+"),
    ASSIGN(5, "="),
    BINARY(6, "[-+/*]"),
    SEPARATOR(7, "[,]"),
    VARIABLE(1, "[a-zA-Z_][a-zA-Z_0-9]*"),
    //Special
    BLOCK(-1, "NULL"),
    EOF(-2, "EOF");

    private String pattern;
    private int id;

    private TokenType(int id, String p) {
        this.id = id;
        this.pattern = p;
    }

    public String getPattern() {
        return this.pattern;
    }

    public int getId() {
        return this.id;
    }
}