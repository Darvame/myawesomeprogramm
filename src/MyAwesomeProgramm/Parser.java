package MyAwesomeProgramm;

import java.util.ArrayList;
import java.util.ListIterator;

public class Parser {
    private ListIterator<Token> tokenList;
    private ExecutionLine executionLine;

    Parser(ArrayList<Token> tokenList, ExecutionLine executionLine) {
        this.executionLine = executionLine;
        this.tokenList = tokenList.listIterator();
    }

    public void parse() {
        while(this.tokenList.hasNext()) {
            if (this.expression())
                return;
        }
    }

    private Token nextToken() {
        if (tokenList.hasNext())
            return tokenList.next();

        return new Token(TokenType.EOF, TokenType.EOF.name());
    }

    private void write(Token t, boolean isChunk) {
        if (!(isChunk)) this.executionLine.currentCommand.add(t);
        else this.executionLine.currentChunk.add(t);
    }

    private void write(int addr, boolean isChunk) {
        if (!(isChunk)) this.executionLine.currentCommand.add(addr);
        else this.executionLine.lastChunk.add(addr);
    }

    private boolean assrt(int e) {
        this.back();

        Token t = this.nextToken();

        System.out.println(t.toString() + "EXPERCTED: " + e);

        return true;
    }

    private void back() {
        this.tokenList.previous();
    }

    private boolean rightValue(boolean isChunk) {
        Token t;

        while(true) {
            t = this.nextToken();

            if (!(t.isValid(1) || t.isValid(2) || t.isValid(4)))
                return assrt(0);

            if (t.isValid(2)) {
                this.write(this.executionLine.newChunk(), isChunk);
                if (rightValue(true))
                    return true;

                if (!(nextToken().isValid(3)))  //close bracket
                    return assrt(3);

                this.executionLine.closeChunk();
            } else {
                this.write(t, isChunk);
            }

            t = nextToken();
            if (!(t.isValid(6)))// BINARY
                break;

            this.write(t, isChunk);
        }

        this.back();
        return false;
    }

    private boolean readArgs() {
        while(true) {
            this.write(this.executionLine.newChunk(), false);
            if (rightValue(true))
                return true;

            this.executionLine.closeChunk();
            //System.out.print(nextToken().toString());
            if (!(this.nextToken().isValid(7))) //sep
                break;
        }

        //this.nextToken();
        return false;
    }

    private boolean expression() {
        Token t = this.nextToken();
        //Variable + OPTION + RIGHT_VALUE

        if (!(t.isValid(1))) {
            if (this.tokenList.hasNext())
                return assrt(1);
            else
                return true; //Variable

        }

        String arg = t.getValue();

        //Determ option
        //Left bracket -> FUNCTION
        //assign -> ASSIGN EXPERISSION

        t = this.nextToken();

        if (t.isValid(5)) { // assign
            executionLine.newCommand(0, arg);
            if (rightValue(false))
                return true;
        } else if (t.isValid(2)) { // function
            executionLine.newCommand(1, arg);
            if (readArgs())
                return true;
        }
        else
            return true;

        return false; //all good
    }
}