package MyAwesomeProgramm;

import java.util.ArrayList;

public class Chunk {
    private ArrayList<Token> list;

    Chunk() {
        this.list = new ArrayList<Token>();
    }

    public ArrayList<Token> getList() {
        return list;
    }

    public void add(Token token) {
        this.list.add(token);
    }

    public void add(int chunkAddress) {
        this.list.add(new Token(TokenType.BLOCK, Integer.toString(chunkAddress)));
    }
}