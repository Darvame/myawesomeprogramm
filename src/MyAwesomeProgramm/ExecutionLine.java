package MyAwesomeProgramm;

import java.util.ArrayList;
import java.util.ListIterator;

public class ExecutionLine {
    private ArrayList<Chunk> chunkPosition;
    private ArrayList<Command> commandList;
    private ArrayList<Chunk> chunkList;

    public Chunk currentChunk;
    public Command currentCommand;
    public Chunk lastChunk;

    ExecutionLine() {
        this.chunkList = new ArrayList<Chunk>();
        this.chunkPosition = new ArrayList<Chunk>();
        this.commandList = new ArrayList<Command>();
    }

    public void newCommand(int commandType, String arg) {
        this.currentCommand = new Command(commandType, arg);
        commandList.add(this.currentCommand);
    }

    public int newChunk() {
        this.lastChunk = this.currentChunk;
        this.currentChunk = new Chunk();

        this.chunkList.add(this.currentChunk);
        this.chunkPosition.add(this.currentChunk);

        return this.chunkList.size() - 1;
    }

    public void closeChunk() {
        this.chunkPosition.remove(this.chunkPosition.size() - 1);

        if (this.chunkPosition.size() > 0)
            this.currentChunk = this.chunkPosition.get(this.chunkPosition.size() - 1);
    }

    public ArrayList<Token> getChunk(int chunkId) {
        return chunkList.get(chunkId).getList();
    }

    public ListIterator<Command> getCommandList() {
        return commandList.listIterator();
    }
}
