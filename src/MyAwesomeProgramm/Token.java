package MyAwesomeProgramm;

public class Token {
    private TokenType type;
    private String value;
    private double option;

    Token(TokenType t, String v) {
        this.type = t;
        this.value = v;
    }

    public String getValue() {
        return this.value;
    }

    public boolean isValid(int expected) {
        return (this.type.getId() == expected);
    }

    public void setOption(double o) {
        this.option = o;
    }

    public double getOption() {
        return this.option;
    }

    public String toString() {
        return this.toString("<%s> %s");
    }

    public String toString(String p) {
        return (String.format(p, this.type, this.value));
    }
}
